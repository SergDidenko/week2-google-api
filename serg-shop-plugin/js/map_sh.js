/**
 * Created by sergey on 14.02.17.
 */
function initMap() {
    var mapDiv = document.getElementById('map');
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var data = JSON.parse(data_sh.data);
    var geocoder = new google.maps.Geocoder();
    var address;
    for (var i = 0; i < data.length; i++) {
        address = data[i].address;
        geocoder.geocode({
            "address": address
        }, func(i));
    }
    function func(i) {
        return function (results, status) {
            var contentString = '<div id="content">' +
                '<div id="siteNotice">' +
                '</div>' +
                '<h1 id="firstHeading" class="firstHeading">' + data[i].name + '</h1>' +
                '<div id="bodyContent">' +
                '<p>' + data[i].description + '</p>' +
                '<p>Address : ' + data[i].address + '</p>' +
                '</div>' +
                '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    infowindow.setPosition(pos);
                    map.setCenter(pos);
                }, function () {
                    handleLocationError(true, infowindow, map.getCenter());
                });
            }
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                title: data[i].name
            });
            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });
        };
    }

}
