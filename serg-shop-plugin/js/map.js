    function initMap() {
        var mapDiv = document.getElementById('map');
        var map;
        var data = JSON.parse( data_for_maps.data );
        var address = data.address;
        var geocoder = new google.maps.Geocoder();
        // Get LatLng information by name
        geocoder.geocode({
            "address": address
        }, function(results, status){
            map = new google.maps.Map(mapDiv, {
                // Center map (but check status of geocoder)
                center: results[0].geometry.location,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });

            var contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h1 id="firstHeading" class="firstHeading">'+ data.name +'</h1>'+
                '<div id="bodyContent">'+
                '<p>'+ data.description +'</p>'+
                '<p>Address : ' + data.address + '</p>'+
                '</div>'+
                '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                title: data.name
            });
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
        });
    }


