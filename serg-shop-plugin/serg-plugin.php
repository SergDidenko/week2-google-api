<?php
/*
Plugin Name: Serg Shop Plugin
Plugin URI: http://localhost
Description:
Version: 1.0
Author: Sergey
Author URI:
*/
// Our custom post type function

function create_posttype() {

	//settings for register_post_type

	$labels = array(
		'name'               => __( 'Shops' ),
		'singular_name'      => __( 'Shop' ),
		'menu_name'          => __( 'Shops' ),
		'name_admin_bar'     => __( 'Shop' ),
		'add_new'            => __( 'Add New' ),
		'add_new_item'       => __( 'Add New Shop' ),
		'new_item'           => __( 'New Shop' ),
		'edit_item'          => __( 'Edit Shop' ),
		'view_item'          => __( 'View Shop' ),
		'all_items'          => __( 'All Shops' ),
		'search_items'       => __( 'Search Shops' ),
		'parent_item_colon'  => __( 'Parent Shops:' ),
		'not_found'          => __( 'No events found.' ),
		'not_found_in_trash' => __( 'No events found in Trash.' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Plugin  should create new post type with different additional parameters' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'plugin-shop' ),
		'capability_type'    => __( 'post' ),
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 10,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'custom fields' ),
		'taxonomies'         => array( 'shop_category' )
	);
	register_post_type( 'shop', $args );
}

// Init is the hook which we use.

add_action( 'init', 'create_posttype' );

function shop_meta_box() {
	add_meta_box(
		'shop_meta_box',
		__( 'Shop - additional information' ),
		'show_my_shop_metabox',
		'shop',
		'normal',
		'high' );
}

add_action( 'add_meta_boxes', 'shop_meta_box' );

$shop_meta_fields = array(
	array(
		'label' => __( 'Name' ),
		'desc'  => __( 'Add name.' ),
		'id'    => 'add_name',
		'type'  => 'text'
	),
	array(
		'label' => __( 'Description' ),
		'desc'  => __( 'Add description of the shop' ),
		'id'    => 'add_description',
		'type'  => 'textarea'
	),
	array(
		'label' => __( 'Address' ),
		'desc'  => __( 'Add address.' ),
		'id'    => 'add_address',
		'type'  => 'text'
	),
);

function show_my_shop_metabox() {
	global $shop_meta_fields;
	global $post;
	echo '<input type="hidden" name="custom_meta_box_nonce" value="' . wp_create_nonce( basename( __FILE__ ) ) . '" />';

	echo '<table class="form-table">';
	foreach ( $shop_meta_fields as $field ) {
		$meta = get_post_meta( $post->ID, $field['id'], true );
		echo '<tr>
                <th><label for="' . $field['id'] . '">' . $field['label'] . '</label></th>
                <td>';
		switch ( $field['type'] ) {
			case 'text':
				echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="30" />
					        <br /><span class="description">' . $field['desc'] . '</span>';
				break;
			case 'textarea':
				echo '<textarea name="' . $field['id'] . '" id="' . $field['id'] . '" rows="7" cols="33">' . $meta . '</textarea>';
				break;
		}
		echo '</td></tr>';
	}
	echo '</table>';
}

function save_my_shop_meta_fields( $post_id ) {
	global $shop_meta_fields;

	if ( ! wp_verify_nonce( $_POST['custom_meta_box_nonce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}
	if ( 'shop' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return $post_id;
		}
	} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	foreach ( $shop_meta_fields as $field ) {
		$old = get_post_meta( $post_id, $field['id'], true );
		$new = $_POST[ $field['id'] ];
		if ( $new && $new != $old ) {
			update_post_meta( $post_id, $field['id'], $new );
		} elseif ( '' == $new && $old ) {
			delete_post_meta( $post_id, $field['id'], $old );
		}
	}
}

add_action( 'save_post', 'save_my_shop_meta_fields' );

add_filter( 'single_template', 'my_custom_template' );

function my_custom_template( $single ) {
	global $post;
	if ( $post->post_type == "shop" ) {
		if ( file_exists( plugin_dir_path( __FILE__ ) . '/single-plugin-shop.php' ) ) {
			return plugin_dir_path( __FILE__ ) . '/single-plugin-shop.php';
		}
	}
	wp_reset_postdata();

	return $single;
}


add_action( 'wp_enqueue_scripts', 'my_script_method' );

function my_script_method() {
	global $post;
	if ( is_singular( 'shop' ) ) {
		$data                = array();
		$data['title']       = $post->post_title;
		$data['name']        = get_post_meta( get_the_ID(), 'add_name', true );
		$data['description'] = get_post_meta( get_the_ID(), 'add_description', true );
		$data['address']     = get_post_meta( get_the_ID(), 'add_address', true );
		$data                = json_encode( $data );
		wp_enqueue_script( 'map', plugin_dir_url( __FILE__ ) . 'js/map.js' );
		wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?
		key=AIzaSyAnCcmvPAxV3BfxH9ZaVjIHcO7BAjqMkKo&callback=initMap', '', '', true );
		wp_localize_script( 'map', 'data_for_maps', array( 'data' => $data ) );
	}
	if ( has_shortcode( $post->post_content, 'shortcode_shop' ) ) {
		$arg     = array(
			'post_type' => 'shop',
		);
		$query   = new WP_Query( $arg );
		$data_sh = array();
		$i       = 0;
		while ( $query->have_posts() ) {
			$query->the_post();
			$data_sh[ $i ]['name']        = get_post_meta( get_the_ID(), 'add_name', true );
			$data_sh[ $i ]['description'] = get_post_meta( get_the_ID(), 'add_description', true );
			$data_sh[ $i ]['address']     = get_post_meta( get_the_ID(), 'add_address', true );
			$i ++;
		}
		wp_reset_postdata();
		$data_sh = json_encode( $data_sh );
		wp_enqueue_script( 'map_sh', plugin_dir_url( __FILE__ ) . 'js/map_sh.js' );
		wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?
		key=AIzaSyAnCcmvPAxV3BfxH9ZaVjIHcO7BAjqMkKo&callback=initMap', '', '', true );
		wp_localize_script( 'map_sh', 'data_sh', array( 'data' => $data_sh ) );
	}
}

wp_register_style( 'style-shop', plugin_dir_url( __FILE__ ) . "css/style.css" );
wp_enqueue_style( 'style-shop' );

require 'shortcode_shop.php';
